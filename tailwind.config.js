/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./public/**/*.{html,js,php}",
    "./node_modules/tw-elements/dist/js/**/*.js",
  ],
  theme: {
    extend: {
      colors: {
        primaryText: "#354052",
        secondaryText: "#939393",
        secondaryTextBlue: "#036ADF",
        primaryDetailText: "#0F345E",
        secondaryDetailText: "#1089C0",
        primaryBtn: "#FFA110",
        onDetailText: "#FFA110",
        offDetailText: "#577280",
        primaryBackgroundDark: "#0F345E",
        secondaryBackgroundDark: "#023773",
        primary: {
          100: '#1176C4',
          200: '#1089C0',
          300: '#6BD1FF',
          400: '#0F4B89',
          500: '#036ADF',
          600: '#A3CEFF',
          700: '#00CAFF',
          800: '#D6F6FF',
          900: 'rgba(15, 52, 94, 0.8)'
      },
      secondary: {
        100: '#FFA110',
        200: '#FFE7C1',
      },
      tertiary: {
          100: '#E906BB',
          200: '#FFCAFA',
      },
      neutral: {
          10: '#EDEEEE',
          20: '#FAFAFA',
          30: '#92989B',
          90: '#939393',
      },
      gray: {
          100: '#FAFAFA',
      }
      },
      fontFamily: {
        Open_Sans: ["Open Sans"],
        Poppins: ["Poppins"],
      },
    },
  },
  plugins: [require("tw-elements/dist/plugin")],
};
